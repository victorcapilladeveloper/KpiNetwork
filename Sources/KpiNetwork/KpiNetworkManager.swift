import Foundation

public typealias KpiResult<T: Codable> = Result<T, Error>

open class KpiNetworkManager {
    
    static var defaultSession: URLSession = .shared
    static var dataTask: URLSessionDataTask?
    
    public static func requestData<T, E: Endpoint>(_ endpoint: E, completion: @escaping (KpiResult<T>) -> Void)  {
        guard let url = URL(string: endpoint.getStringURL()) else {
            fatalError("Could not create URL from the given URL components.")
        }
        dataTask?.cancel()
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = endpoint.method.rawValue
        
        if let params = endpoint.parameters {
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
                urlRequest.httpBody = jsonData
            } catch (let formatError) {
                completion(.failure(formatError))
            }
        }
        
        if let header = endpoint.headers {
            urlRequest.allHTTPHeaderFields = header.dictionary
        }
        
        dataTask = defaultSession.dataTask(with: urlRequest, completionHandler: { data, response, error in
            if let error = error  {
                completion(.failure(error))
            }
            if let data = data {
                do {
                    let parsedData = try JSONDecoder().decode(T.self, from: data)
                    completion(.success(parsedData))
                } catch (let fError) {
                    print(fError)
                    completion(.failure(fError))
                }
            }
        })
        do {dataTask?.resume()}
    }
    
}

